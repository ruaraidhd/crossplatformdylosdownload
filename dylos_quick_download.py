import serial.tools.list_ports
from Tkinter import *
# import tkFileDialog
import tkMessageBox
import ttk
import os, os.path
import datetime

# This is a quick and dirty way of downloading Dylos DC1700 data. 
# It does no conversion to mass concentrations and it's not at all robust.
# USE AT YOUR OWN RISK!


class Application(Frame):
    def download_data(self):
        port = self.COMPORTS.get()
        if port == "":
            tkMessageBox.showerror("Error", "Valid port not selected. Is a Dylos plugged in?")
            return
        ser = serial.Serial(port, timeout=5)
        ser.write("D\r")
        data = ser.readlines()
        filename = "data_downloaded_" + datetime.datetime.now().strftime("%d%m%Y_%H%M%S") + ".csv"
        with open(filename, "wb") as open_file:
            for each in data:
                open_file.write(each)
        ser.close()
        tkMessageBox.showinfo("Downloaded", "Data has been downloaded to: " + filename + ". I'll try to open this directory now.")
        try:
            os.system("open .")
            os.startfile(".")
        except:
            pass


    def get_ports_list(self):
        self.list_of_ports = [x[0] for x in serial.tools.list_ports.comports()]
        print self.list_of_ports

    def create_widgets(self):
        self.COMPORTS = ttk.Combobox(self, values=self.list_of_ports)
        try:
            self.COMPORTS.set(self.list_of_ports[-1])
        except IndexError as e:
            print e
        self.COMPORTS.pack()

        self.DOWNLOAD = Button(self)
        self.DOWNLOAD["text"] = "Download"
        self.DOWNLOAD["command"] = self.download_data
        self.DOWNLOAD.pack({"side": "right"})

    def __init__(self, master=None):
        Frame.__init__(self, master, height=200, width=300)
        self.get_ports_list()
        self.pack(fill=None, expand=False)
        self.create_widgets()

        

if __name__ == '__main__':
    root = Tk()
    root.wm_title("Dylos quick download")
    app = Application(master=root)
    app.mainloop()
    root.destroy()
