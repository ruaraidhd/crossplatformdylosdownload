#Cross Platform Dylos Download v0.1

This small programme downloads data from a Dylos DC1700 to a local file (a CSV file) using a very simple GUI. It has no additional features - no conversion to mass concentration or more sensible units, and small and large particle readings are understated by a factor of 100 (add two trailing zeroes to get them right).

I've tested this on Windows 10 and macOS High Sierra, but as it's a really simple python programme using cross-platform libraries it should work on Linux/BSD too. Let me know if you find it useful!

##Requirements

* Python 2.7 (I promise I'll upgrade soon. Probably.)
* PySerial v3.0

##LICENCE

Copyright 2019 Ruaraidh Dobson (r.p.dobson@stir.ac.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.